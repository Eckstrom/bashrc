#!/bin/bash -xv
##########################################################
# Author: Joey Eckstrom
# Date: 2017-01-12
# A generalized .bashrc
##########################################################

alias_ssh ()
{
        who=$1
        for host in ${@:2}; do
                cmd="alias ${host}='ssh ${who}@${host}'"
                eval ${cmd}
        done
}


jj() {
        jj_help() {
                echo "Usage: jj joiner arg [args...]"
                echo "The joiner must be provided as a command line argument"
        }

        if [ -z "$1" ]; then
                jj_help ; return 1
        fi

        if [ -z "$2" ]; then
                set -- "$1" $(</dev/stdin)
        fi

        delim="$1"
        args=("${@:2}")
        if [ -z "$args" ]; then
                jj_help ; return 1
        fi

        last=$((${#args[@]}-1))
        for i in $(seq 0 $last); do
                printf "%s" "${args[i]}"
                if [ "$i" -lt "$last" ]; then
                        printf "%s" "${delim}"
                fi
        done
        echo
}

complement ()
{
## Relative Set Complement of two files, where each line in the file is an
## element of the set (elements in $2, that are not in $1)
        if [ -z "$1" -o -z "$2" ]; then
                echo "Usage: complement file1 file2"
                echo "Yields the lines in file1 that are not in file2"
                return 1
        fi

        grep -vxF -f "$(pwd)/${2}" "$(pwd)/${1}"
}

intersection ()
{
## Relative Set Intersection of two files, where each line in the file is an
## element of the set (elements in $1, that are also in $2)
        if [ -z "${1}" -o -z "${2}" ]; then
                echo "Usage: intersection file1 file2"
                echo "Yields the lines in file1 that are also in file2"
        fi
        grep -xF -f "$(pwd)/${2}" "$(pwd)/${1}"
}

# Pretty (ugly) prompt
ULINE="\[\e[4m\]"
UNLINE="\[\e[24m\]"
NOCOLOR="\[\e[0m\]"
BCYAN="\[\e[1;36m\]"
BGREEN="\[\e[1;32m\]"
WDIR="${BCYAN}\w]"
USER="${BCYAN}[\u"
HOST="${BGREEN}${ULINE}\h${UNLINE}${BCYAN}"
PS1="${USER}@${HOST} ${WDIR}\$${NOCOLOR} "

# Listing
alias ll='ls -lh --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto'
alias la='ls -lha --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto'
alias grep='grep --color=tty -d skip'

# Monitoring
alias df='df -Th'                         # human-readable sizes
alias du='du -h'                          # "
alias free='free'                         # "
alias ping='ping -c 5'
alias follow='tail -f'
alias resudo='sudo !!'

# Editing
if [ -n "$(which emacs 2>/dev/null)" ]; then  
    EDITOR=$(which emacs) -nw  # Yay, we have emacs!
    alias vimrc=$EDITOR ~/.emacs.d/init.el  # I fixed vim
elif [ -n "$(which vim 2>/dev/null)" ]; then
    EDITOR=$(which vim) -O  # I can make do...
    alias vimrc=$EDITOR ~/.vimrc
    alias vim=$EDITOR
else
    EDITOR=$(which vi)  # God help me...
    alias vimrc=echo "I can't do that, Dave... (No vim found)"
fi
alias e=$EDITOR
alias v=$EDITOR
alias bashrc='$EDITOR ~/.bashrc'

# Make it harder to do stupid stuff
# don't let me clobber files
# use >| to force the clobber
set -o noclobber
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

# Correct my slip-ups
alias sl='ls'
alias celar='clear'
alias xit='exit'
alias exti='exit'
alias vmi=$EDITOR

# Move around
up() {
        where=$(printf '../%.0s' $(seq 1 ${1}))
        eval "cd ${where}"
}

alias back='cd -'
